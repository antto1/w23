import logo from './logo.svg';
import './App.css';
import React from 'react';
import Reporter from './Reporter.js';

function App() {
  return (
    <div >
      <Reporter name="Antero Mertaranta" />

      <Reporter>Löikö mörkö sisään</Reporter>

      <Reporter name="Kevin McGran">
      I know it's a rough time now, but did you at least enjoy playing in the tournament
      </Reporter>
    </div>
  );
}

export default App;
 